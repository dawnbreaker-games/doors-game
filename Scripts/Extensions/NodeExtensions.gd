class_name NodeExtensions

static func GetNode (type, includeInternal = false, root : Node = null):
	if root == null:
		root = GameManager.instance.get_tree().current_scene
	var nodesToSearch : Array[Node]
	nodesToSearch.append(root)
	while nodesToSearch.size() > 0:
		var node = nodesToSearch[0]
		if is_instance_of(node, type):
			return node
		nodesToSearch.append_array(node.get_children(includeInternal))
		nodesToSearch.remove_at(0)
	return null

static func GetNodes (type, includeInternal = false, root : Node = null):
	if root == null:
		root = GameManager.instance.get_tree().current_scene
	var output : Array[Node]
	var nodesToSearch : Array[Node]
	nodesToSearch.append(root)
	while nodesToSearch.size() > 0:
		var node = nodesToSearch[0]
		if is_instance_of(node, type):
			output.append(node)
		nodesToSearch.append_array(node.get_children(includeInternal))
		nodesToSearch.remove_at(0)
	return output
