class_name VectorExtensions

static func GetDeg2Rad (v : Vector2):
	return Vector2(deg_to_rad(v.x), deg_to_rad(v.y))

static func GetRad2Deg (v : Vector2):
	return Vector2(rad_to_deg(v.x), rad_to_deg(v.y))

static func Rotated90 (v : Vector2):
	return Vector2(-v.y, v.x)

static func Rotated270 (v : Vector2):
	return Vector2(v.y, -v.x)

static func Snap (v : Vector2, interval : float):
	return Vector2(MathExtensions.SnapToInterval(v.x, interval), MathExtensions.SnapToInterval(v.y, interval))

static func GetFacingAngle (v : Vector2):
	v = v.normalized()
	return rad_to_deg(atan2(v.y, v.x)) + 90

static func FromFacingAngle (angle : float) -> Vector2:
	angle = deg_to_rad(angle)
	return Vector2(sin(angle), cos(angle))
