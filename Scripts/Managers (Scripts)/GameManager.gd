extends Node2D
class_name GameManager

@export var useHub : bool
@export var worldScale : int
@export var helpScreen : Node2D
@export var gameplayCamera : _Camera2D
@export var helpScreenCamera : _Camera2D
@export var helpLabel : Label
@export var inspectedNodeTransform : Node2D
@export var closeHelpScreenButton : Button
static var currentLevelSceneIndex = 0
static var instance : GameManager
static var paused : bool
static var initialized : bool
static var previousInspectedNodeTransform : Transform2D
static var inspectedNode : Node2D
static var helpText : String
static var config = ConfigFile.new()
static var loading = false
static var justEnteredLevel = false
const SAVE_FILE_PATH = "user://Save Data.cfg"
const CLONE_SCENE = preload("res://Scenes/Objects (Scenes)/Clone.tscn")

func _ready ():
	if instance == null:
		instance = self
	paused = true
	CloseHelp ()
	if !initialized:
		initialized = true
		if !useHub:
			currentLevelSceneIndex = 1
		helpText = helpLabel.text
		var renderers = NodeExtensions.GetNodes(Renderer)
		for renderer in renderers:
			renderer.visible = false
		helpScreen.visible = true
		helpScreenCamera.make_current()
		paused = true
		if useHub:
			Load ()
	elif loading:
		loading = false
		Load ()

static func NextLevel ():
	currentLevelSceneIndex += 1
	var scenePath = "res://Scenes/" + str(currentLevelSceneIndex) + ".tscn"
	if !ResourceLoader.exists(scenePath):
		currentLevelSceneIndex -= 1
		return
	SetScene (scenePath)

static func PreviousLevel ():
	currentLevelSceneIndex -= 1
	var scenePath = "res://Scenes/" + str(currentLevelSceneIndex) + ".tscn"
	if (currentLevelSceneIndex == 0 && !instance.useHub) || !ResourceLoader.exists(scenePath):
		currentLevelSceneIndex += 1
		return
	SetScene (scenePath)

static func SetLevel (levelSceneIndex : int):
	currentLevelSceneIndex = levelSceneIndex
	SetScene ("res://Scenes/" + str(levelSceneIndex) + ".tscn")

static func SetScene (scenePath : String):
	instance.get_tree().change_scene_to_file(scenePath)

static func AddScene (scenePath : String):
	var scene = load(scenePath)
	var clone = scene.instantiate()
	instance.get_tree().current_scene.add_child(clone)

func OnSetScene ():
	if get_tree() == null:
		return
	Door.instances.clear()
	Wall.instances.clear()
	Clone.instances.clear()
	Weightpad.instances.clear()
	LevelPortal.instances.clear()
	Player.instance = %Player
	_Camera2D.instance = %_Camera2D
	Door.instances.append_array(get_tree().get_nodes_in_group("doors"))
	Wall.instances.append_array(get_tree().get_nodes_in_group("walls"))
	Clone.instances.append_array(get_tree().get_nodes_in_group("clones"))
	Weightpad.instances.append_array(get_tree().get_nodes_in_group("weightpads"))
	LevelPortal.instances.append_array(get_tree().get_nodes_in_group("levelPortals"))

func SaveScene (rootNode = null, resourcePath = ""):
	if rootNode == null:
		rootNode = instance.get_tree().current_scene
	var scene = PackedScene.new()
	var error = scene.pack(rootNode)
	if !error:
		print("Packed scene")
		error = ResourceSaver.save(scene, resourcePath)
		if !error:
			print("Saved scene")
		else:
			print(error)
	else:
		print(error)

static func OpenHelp ():
	paused = true
	for renderer in Renderer.instances:
		renderer.canvasItem.visible = false
	instance.helpScreen.visible = true
	if inspectedNode != null:
		var renderers = NodeExtensions.GetNodes(Renderer, false, inspectedNode)
		for renderer in renderers:
			renderer.visible = true
		previousInspectedNodeTransform = inspectedNode.global_transform
		inspectedNode.global_transform = instance.inspectedNodeTransform.global_transform
	else:
		instance.helpLabel.text = helpText
	instance.helpScreenCamera.make_current()

static func CloseHelp ():
	if !paused:
		return
	paused = false
	for renderer in Renderer.instances:
		renderer.canvasItem.visible = true
	instance.helpScreen.visible = false
	if inspectedNode != null:
		inspectedNode.global_transform = previousInspectedNodeTransform
		inspectedNode = null
	instance.gameplayCamera.make_current()

func Save ():
	if Player.undoing:
		return
	config.set_value("", "level", currentLevelSceneIndex)
	OnSetScene ()
	if currentLevelSceneIndex == 0:
		SetConfigValue ("", "playerPositionInHub", Player.instance.global_position)
		SetConfigValue ("", "playerRotationInHub", Player.instance.global_rotation_degrees)
	else:
		SetConfigValue ("", "playerPosition", Player.instance.global_position)
		SetConfigValue ("", "playerRotation", Player.instance.global_rotation_degrees)
	SetConfigValue ("", "playerAffectedByTimeeDoors", Player.instance.affectedByTimeDoors)
	var doorStates : Array
	var timeDoorInfos : Array
	for door in Door.instances:
		doorStates.append(door.open)
		var timeDoor = door as TimeDoor
		if timeDoor != null:
			timeDoorInfos.append([ timeDoor.savedDoorStates, timeDoor.savedClonePathMovesDicts, timeDoor.savedClonePathPositionsDicts, timeDoor.savedCloneDoors, timeDoor.savedPlayerPosition, timeDoor.savedPlayerRotation, timeDoor.savedPlayerPathMovesDict, timeDoor.savedPlayerPathPositionsDict ])
	SetConfigValue (str(currentLevelSceneIndex), "doorStates", doorStates)
	SetConfigValue (str(currentLevelSceneIndex), "timeDoorInfos", timeDoorInfos)
	var cloneInfos : Array
	for clone in Clone.instances:
		cloneInfos.append([ clone.global_position, clone.global_rotation_degrees, clone.affectedByTimeDoors, clone.pathMovesDict, clone.pathPositionsDict, clone.nextMoveIndex, Door.instances.find(clone.spawnedBy) ])
	SetConfigValue (str(currentLevelSceneIndex), "cloneInfos", cloneInfos)
	config.save(SAVE_FILE_PATH)
	justEnteredLevel = false

func Load ():
	var error = config.load(SAVE_FILE_PATH)
	if error == OK && config.has_section(str(currentLevelSceneIndex)):
		var levelIndex = config.get_value("", "level")
		if currentLevelSceneIndex != levelIndex:
			if useHub:
				loading = true
			SetLevel (levelIndex)
			return
		OnSetScene ()
		if currentLevelSceneIndex == 0:
			Player.instance.global_position = GetConfigValue("", "playerPositionInHub", Player.instance.global_position)
			Player.instance.global_rotation_degrees = GetConfigValue("", "playerRotationInHub", Player.instance.global_rotation_degrees)
			_Camera2D.instance.global_position = Player.instance.global_position
		elif !justEnteredLevel:
			Player.instance.global_position = GetConfigValue("", "playerPosition", Player.instance.global_position)
			Player.instance.global_rotation_degrees = GetConfigValue("", "playerRotation", Player.instance.global_rotation_degrees)
		Player.instance.affectedByTimeDoors = GetConfigValue("", "playerAffectedByTimeeDoors", Player.instance.affectedByTimeDoors)
		if !Player.instance.affectedByTimeDoors:
			Player.instance.get_child(0).use_parent_material = true
		var doorStates = GetConfigValue(str(currentLevelSceneIndex), "doorStates", [])
		if doorStates.size() == 0:
			return
		for i in range(Door.instances.size()):
			var door = Door.instances[i]
			door.initOpen = door.open
			var portalDoorIndex = 0
			var timeDoorIndex = 0
			door.SetOpen (doorStates[i])
			if door is PortalDoor:
				door.global_rotation_degrees = GetConfigValue(str(currentLevelSceneIndex), "portalDoorRotations")[portalDoorIndex]
				portalDoorIndex += 1
			else:
				var timeDoor = door as TimeDoor
				if timeDoor != null:
					var timeDoorInfo = GetConfigValue(str(currentLevelSceneIndex), "timeDoorInfos")[timeDoorIndex]
					timeDoor.savedDoorStates = timeDoorInfo[0]
					timeDoor.savedClonePathMovesDicts = timeDoorInfo[1]
					timeDoor.savedClonePathPositionsDicts = timeDoorInfo[2]
					timeDoor.savedCloneDoors = timeDoorInfo[3]
					timeDoor.savedPlayerPosition = timeDoorInfo[4]
					timeDoor.savedPlayerRotation = timeDoorInfo[5]
					timeDoor.savedPlayerPathMovesDict = timeDoorInfo[6]
					timeDoor.savedPlayerPathPositionsDict = timeDoorInfo[7]
					timeDoorIndex += 1
		var cloneInfos = GetConfigValue(str(currentLevelSceneIndex), "cloneInfos")
		for cloneInfo in cloneInfos:
			var clone = GameManager.CLONE_SCENE.instantiate()
			clone.global_position = cloneInfo[0]
			clone.global_rotation_degrees = cloneInfo[1]
			clone.affectedByTimeDoors = cloneInfo[2]
			if !clone.affectedByTimeDoors:
				clone.get_child(0).use_parent_material = true
			clone.pathMovesDict = cloneInfo[3]
			clone.pathPositionsDict = cloneInfo[4]
			clone.nextMoveIndex = cloneInfo[5]
			clone.spawnedBy = Door.instances[cloneInfo[6]] as CloneDoor
			clone.nextMove = clone.pathMovesDict[clone.spawnedBy][clone.nextMoveIndex]
			if clone.nextMoveIndex == 0:
				clone.visible = false
			instance.get_tree().current_scene.add_child(clone)
	if justEnteredLevel:
		Save ()
	justEnteredLevel = false
	Player.undoing = false

func _notification (notification):
	if notification == NOTIFICATION_WM_CLOSE_REQUEST:
		Quit ()

func Quit ():
	Save ()
	get_tree().quit()

static func SetPaused (pause : bool):
	paused = pause

static func SetConfigValue (section : String, key : String, value):
	config.set_value(section, key, value)
	if justEnteredLevel:
		config.set_value("reset", key, value)

static func GetConfigValue (section : String, key : String, defaultValue = null):
	if justEnteredLevel:
		return config.get_value("reset", key, defaultValue)
	else:
		return config.get_value(section, key, defaultValue)
