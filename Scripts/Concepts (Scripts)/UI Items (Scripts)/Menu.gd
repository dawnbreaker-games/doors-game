extends UIItem
class_name Menu

@export var open : bool
@export var pauseOnOpen : bool
@export var unpauseOnClose : bool
var focusables : Array[Control]

func _ready ():
	super._ready ()
	focusables.append_array(NodeExtensions.GetNodes(Button, false, self))
	if open:
		Open ()
	else:
		Close ()

func Open ():
	open = true
	visible = true
	for focusable in focusables:
		focusable.focus_mode = FocusMode.FOCUS_ALL
	if pauseOnOpen:
		GameManager.paused = true

func Close ():
	open = false
	visible = false
	for focusable in focusables:
		focusable.focus_mode = FocusMode.FOCUS_NONE
	if unpauseOnClose:
		GameManager.paused = false
