extends Menu
class_name PauseMenu

static var instance : PauseMenu

func _ready ():
	super._ready ()
	instance = self

func Open ():
	super.Open ()
	global_position = _Camera2D.instance.global_position - size / 2
	GameManager.instance.closeHelpScreenButton.disabled = true

func Close ():
	super.Close ()
	GameManager.instance = get_tree().current_scene.get_node("GameManager")
	GameManager.instance.closeHelpScreenButton.disabled = false

func OpenSettingsMenu ():
	Close ()
	SettingsMenu.instance.Open ()

func Quit ():
	GameManager.instance.Quit ()
