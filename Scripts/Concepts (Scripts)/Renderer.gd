extends CanvasItem
class_name Renderer

@export var canvasItem : CanvasItem
static var instances : Array[Renderer]

func _ready ():
	instances.append(self)
	tree_exiting.connect(OnTreeExiting)

func OnTreeExiting ():
	instances.erase(self)
