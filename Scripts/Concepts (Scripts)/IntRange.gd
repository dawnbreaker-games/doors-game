extends Node
class_name IntRange

@export var min : int
@export var max : int

static func New (min : int, max : int):
	var output : IntRange
	output.min = min
	output.max = max
	return output

func Contains (value : int, containsMin = true, containsMax = true):
	var greaterThanMin = min < value
	if containsMin && min == value:
		greaterThanMin = true
	var lessThanMax = value < max
	if containsMax && value == max:
		lessThanMax = true
	return greaterThanMin && lessThanMax
