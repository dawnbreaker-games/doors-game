extends Node2D
class_name Player

@export var rayCast : RayCast2D
var previousWeightpad : Weightpad
var affectedByTimeDoors = true
var pathMovesDict : Dictionary
var pathPositionsDict : Dictionary
static var moves : Array[Vector2i]
static var instance : Player
static var undoing = false
static var resetting = false
static var initPosition : Vector2i
static var wantsToBePushed : bool

func _ready ():
	if instance == null:
		instance = self
		initPosition = global_position

func _process (delta):
	if undoing:
		for move in moves:
			Move (move)
		undoing = false

func _input (event : InputEvent):
	if GameManager.paused:
		return
	if event is InputEventKey:
		if Input.is_action_just_pressed("Move Left"):
			Move (Vector2.LEFT)
		elif Input.is_action_just_pressed("Move Right"):
			Move (Vector2.RIGHT)
		elif Input.is_action_just_pressed("Move Down"):
			Move (Vector2.DOWN)
		elif Input.is_action_just_pressed("Move Up"):
			Move (Vector2.UP)
		elif Input.is_action_just_pressed("Wait"):
			Move (Vector2.ZERO)
		elif Input.is_action_just_pressed("Reset"):
			if %GameManager.useHub:
				Player.undoing = true
				GameManager.config.set_value("", "level", GameManager.currentLevelSceneIndex)
				GameManager.config.save(GameManager.SAVE_FILE_PATH)
				GameManager.justEnteredLevel = true
				%GameManager.Load ()
			else:
				get_tree().reload_current_scene()
				global_position = initPosition
			moves.clear()
		elif Input.is_action_just_pressed("Undo") && moves.size() > 0:
			Undo ()
		elif Input.is_action_just_pressed("Next Level"):
			moves.clear()
			GameManager.NextLevel ()
		elif Input.is_action_just_pressed("Previous Level"):
			moves.clear()
			GameManager.PreviousLevel ()
		#elif Input.is_action_just_pressed("Pause"):
			#if PauseMenu.instance.open:
				#PauseMenu.instance.Close ()
			#else:
				#PauseMenu.instance.Open ()
	elif event is InputEventMouseButton:
		if Input.is_action_just_pressed("Inspect"):
			rayCast.global_position = get_global_mouse_position()
			for i in range(2, 33):
				rayCast.set_collision_mask_value(i, false)
			for i in range(2, 33):
				rayCast.force_raycast_update()
				var collider = rayCast.get_collider()
				if collider == null:
					rayCast.set_collision_mask_value(i, true)
				else:
					Inspect (collider)
					return

func Inspect (inspectable : Inspectable):
	%GameManager.helpLabel.text = inspectable.text
	GameManager.inspectedNode = inspectable.get_parent()
	GameManager.OpenHelp.call_deferred()

func Undo ():
	undoing = true
	moves.remove_at(moves.size() - 1)
	if GameManager.instance.useHub:
		for door in Door.instances:
			door.SetOpen (door.initOpen)
		for clone in Clone.instances:
			get_tree().current_scene.remove_child(clone)
			clone.queue_free()
		Clone.instances.clear()
		global_position = initPosition
		for move in moves:
			Move (move)
		if GameManager.currentLevelSceneIndex == 0:
			%_Camera2D.global_position = global_position
		undoing = false
	else:
		get_tree().reload_current_scene()

func Move (direction : Vector2i):
	if instance == self:
		%GameManager.OnSetScene ()
		for door in Door.instances:
			if str(door) != "<Freed Object>":
				if door.open && global_position == door.global_position:
					Undo ()
					return false
	var cloneDoor : CloneDoor
	if direction != Vector2i.ZERO:
		global_rotation_degrees = VectorExtensions.GetFacingAngle(direction)
		var move = direction * GameManager.instance.worldScale
		for wall in Wall.instances:
			if str(wall) != "<Freed Object>":
				if wall.global_position == global_position + Vector2(move):
					OnMoved (direction, true, cloneDoor)
					return false
		var shouldMove = true
		for door in Door.instances:
			if str(door) != "<Freed Object>":
				var toDoorCenter = move / 2
				var doorCenter = global_position + Vector2(toDoorCenter)
				var tangent = VectorExtensions.Rotated90(toDoorCenter)
				if (door.startPosition.is_equal_approx(doorCenter + tangent) && door.endPosition.is_equal_approx(doorCenter - tangent)) || (door.startPosition.is_equal_approx(doorCenter - tangent) && door.endPosition.is_equal_approx(doorCenter + tangent)):
					if !door.open:
						OnMoved (direction, true, cloneDoor)
						return false
					else:
						var enteredSide = door.to_local(doorCenter + Vector2(direction)).x
						if door is PortalDoor:
							var teleportTo = Vector2i(door.other.to_global(Vector2.RIGHT * enteredSide * %GameManager.worldScale / 2))
							var clone = self as Clone
							if clone != null && teleportTo != pathPositionsDict[clone.spawnedBy][clone.nextMoveIndex]:
								return false
							if IsAPlayerAtSpot(teleportTo):
								OnMoved (direction, true, cloneDoor)
								return false
							global_position = teleportTo
							door.global_rotation_degrees += 180
							shouldMove = false
						elif IsAPlayerAtSpot(global_position + Vector2(move)):
							OnMoved (direction, true, cloneDoor)
							return false
						elif door is TrapDoor:
							door.SetOpen (false)
						elif door is TimeDoor:
							global_position += Vector2(move)
							shouldMove = false
							if enteredSide == 1:
								door.Save ()
							else:
								door.Load ()
						elif door is CloneDoor:
							global_position += Vector2(move)
							shouldMove = false
							cloneDoor = door
							if enteredSide == 1:
								door.StartRecording (self)
							else:
								door.StartPlayback (self)
						elif door is AntiTimeDoor:
							affectedByTimeDoors = false
							get_child(0).use_parent_material = true
					break
		if shouldMove:
			if IsAPlayerAtSpot(global_position + Vector2(move)):
				OnMoved (direction, true, cloneDoor)
				return false
			global_position += Vector2(move)
		global_position = VectorExtensions.Snap(global_position, GameManager.instance.worldScale)
		var onWeightpad = false
		for weightpad in Weightpad.instances:
			if str(weightpad) != "<Freed Object>":
				if weightpad.global_position == global_position:
					if previousWeightpad == null || previousWeightpad != weightpad:
						if previousWeightpad != null && previousWeightpad.toggleOnExit:
							for toggleDoor in previousWeightpad.toggleDoors:
								toggleDoor.SetOpen (!toggleDoor.open)
						for toggleDoor in weightpad.toggleDoors:
							toggleDoor.SetOpen (!toggleDoor.open)
						if weightpad.flipDoors:
							for door in Door.instances:
								door.global_rotation_degrees += 180
						previousWeightpad = weightpad
					onWeightpad = true
					break
		if !onWeightpad && previousWeightpad != null:
			if previousWeightpad.toggleOnExit:
				for toggleDoor in previousWeightpad.toggleDoors:
					toggleDoor.SetOpen (!toggleDoor.open)
			previousWeightpad = null
	OnMoved (direction, false, cloneDoor)
	return true

func OnMoved (triedDirection : Vector2i, crashed : bool, triedToGoThroughCloneDoor : CloneDoor):
	if instance != self:
		return
	for key in pathMovesDict.keys():
		if key != triedToGoThroughCloneDoor:
			pathMovesDict[key].append(triedDirection * BoolExtensions.To01(!crashed))
			pathPositionsDict[key].append(Vector2i(global_position))
	for clone in Clone.instances:
		if str(clone) != "<Freed Object>":
			clone.NextMove ()
	if !undoing:
		moves.append(triedDirection)
		if %GameManager.useHub:
			%GameManager.Save ()
	if GameManager.currentLevelSceneIndex == 0:
		%_Camera2D.global_position = global_position
	if global_position == %End.global_position:
		if %GameManager.useHub:
			if GameManager.config.has_section("0"):
				GameManager.config.set_value("", "level", 0)
				GameManager.config.save(GameManager.SAVE_FILE_PATH)
				%GameManager.Load ()
			else:
				%GameManager.SetLevel (0)
		else:
			moves.clear()
			GameManager.NextLevel ()
		return
	else:
		for levelPortal in LevelPortal.instances:
			if str(levelPortal) != "<Freed Object>":
				if global_position == levelPortal.global_position:
					if GameManager.config.has_section(str(levelPortal.levelIndex)):
						GameManager.config.set_value("", "level", levelPortal.levelIndex)
						GameManager.config.save(GameManager.SAVE_FILE_PATH)
						GameManager.justEnteredLevel = true
						%GameManager.Load ()
					else:
						%GameManager.SetLevel (levelPortal.levelIndex)
					return

static func IsAPlayerAtSpot (spot : Vector2i):
	var players : Array[Player]
	players.append(instance)
	players.append_array(Clone.instances)
	for player in players:
		if player.visible && player.global_position == Vector2(spot):
			return true
	return false
