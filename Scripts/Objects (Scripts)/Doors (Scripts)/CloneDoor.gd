extends Door
class_name CloneDoor

func StartRecording (player : Player):
	var pathMoves : Array[Vector2i]
	pathMoves.append(Vector2i(global_position + to_local(global_position + Vector2.RIGHT) * GameManager.instance.worldScale / 2))
	player.pathMovesDict[self] = pathMoves
	player.pathPositionsDict[self] = pathMoves.duplicate()

func StartPlayback (player : Player):
	var pathMoves = player.pathMovesDict.get(self)
	if pathMoves == null:
		return
	var clone = GameManager.CLONE_SCENE.instantiate()
	clone.scale = Vector2.ONE * Player.instance.scale
	clone.visible = false
	clone.spawnedBy = self
	clone.nextMove = pathMoves[0]
	clone.global_position = clone.nextMove
	var position = Vector2i(global_position + to_local(global_position + Vector2.RIGHT) * GameManager.instance.worldScale / 2)
	clone.pathMovesDict = player.pathMovesDict.duplicate()
	clone.pathMovesDict[self].append(position)
	clone.pathPositionsDict = player.pathPositionsDict.duplicate()
	clone.pathPositionsDict[self].append(position)
	get_tree().current_scene.add_child(clone)
