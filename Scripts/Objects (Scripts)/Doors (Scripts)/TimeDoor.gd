extends Door
class_name TimeDoor

var savedDoorStates : Array[bool]
var savedClonePathMovesDicts : Array[Dictionary]
var savedClonePathPositionsDicts : Array[Dictionary]
var savedCloneRotations : Array[int]
var savedCloneDoors : Array[CloneDoor]
var savedPlayerPosition : Vector2i
var savedPlayerRotation : int
var savedPlayerPathMovesDict : Dictionary
var savedPlayerPathPositionsDict : Dictionary

func Save ():
	savedDoorStates.clear()
	for door in Door.instances:
		if door.affectedByTimeDoors:
			savedDoorStates.append(door.open)
	savedClonePathMovesDicts.clear()
	savedClonePathPositionsDicts.clear()
	savedCloneRotations.clear()
	savedCloneDoors.clear()
	for clone in Clone.instances:
		if clone.affectedByTimeDoors:
			var pathMovesDict = clone.pathMovesDict.duplicate()
			pathMovesDict[clone.spawnedBy] = pathMovesDict[clone.spawnedBy].slice(clone.nextMoveIndex)
			pathMovesDict[0] = Vector2i(clone.global_position)
			savedClonePathMovesDicts.append(pathMovesDict)
			var pathPositionsDict = clone.pathPositionsDict.duplicate()
			pathPositionsDict[clone.spawnedBy] = pathPositionsDict[clone.spawnedBy].slice(clone.nextMoveIndex)
			pathPositionsDict[0] = Vector2i(clone.global_position)
			savedClonePathPositionsDicts.append(pathPositionsDict)
			savedCloneRotations.append(clone.global_rotation_degrees)
			savedCloneDoors.append(clone.spawnedBy)
	savedPlayerPosition = Vector2i(Player.instance.global_position)
	savedPlayerRotation = Player.instance.global_rotation_degrees
	savedPlayerPathMovesDict = Player.instance.pathMovesDict
	savedPlayerPathPositionsDict = Player.instance.pathPositionsDict

func Load ():
	if savedDoorStates.size() == 0:
		return
	var i = 0
	for door in Door.instances:
		if door.affectedByTimeDoors:
			door.SetOpen (savedDoorStates[i])
			i += 1
	for clone in Clone.instances:
		if clone.affectedByTimeDoors:
			get_tree().current_scene.remove_child(clone)
			clone.queue_free()
	if Player.instance.affectedByTimeDoors:
		Player.instance.global_position = savedPlayerPosition
		Player.instance.global_rotation_degrees = savedPlayerRotation
		Player.instance.pathMovesDict = savedPlayerPathMovesDict
		Player.instance.pathPositionsDict = savedPlayerPathPositionsDict
	if savedClonePathMovesDicts.size() > 0:
		for i2 in range(savedClonePathMovesDicts.size()):
			var clone = GameManager.CLONE_SCENE.instantiate()
			clone.scale = Vector2.ONE * Player.instance.scale
			clone.visible = false
			clone.spawnedBy = savedCloneDoors[i2]
			var pathMovesDict = savedClonePathMovesDicts[i2]
			clone.nextMove = pathMovesDict[clone.spawnedBy][0]
			clone.global_position = clone.nextMove
			clone.pathMovesDict = pathMovesDict
			clone.pathPositionsDict = savedClonePathPositionsDicts[i2]
			clone.global_rotation_degrees = savedCloneRotations[i2]
			get_tree().current_scene.add_child(clone)
			clone.NextMove ()
