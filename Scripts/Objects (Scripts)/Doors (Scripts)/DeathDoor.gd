extends Door
class_name DeathDoor

func SetOpen (open : bool):
	super.SetOpen (open)
	if open && Player.instance.global_position == global_position:
		Player.instance.Undo ()
