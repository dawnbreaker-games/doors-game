extends Door
class_name MetaDoor

@export var toggledDoorsCountRangeToToggleMe : IntRange
@export var toggledDoorsCountRangeToToggleMeLabel : Label

func _ready ():
	super._ready ()
	Update (0)
	
func Update (doorsChangedDelta : int):
	var doorsChanged = GameManager.config.get_value("", "doorsChanged", 0)
	doorsChanged += doorsChangedDelta
	GameManager.config.set_value("", "doorsChanged", doorsChanged)
	GameManager.config.save(GameManager.SAVE_FILE_PATH)
	var toggledDoorsRemainingToMin = toggledDoorsCountRangeToToggleMe.min - doorsChanged
	var toggledDoorsRemainingToMax = toggledDoorsCountRangeToToggleMe.max - doorsChanged
	toggledDoorsCountRangeToToggleMeLabel.text = str(toggledDoorsRemainingToMin) + " to " +  str(toggledDoorsRemainingToMax)
	if toggledDoorsCountRangeToToggleMe.Contains(doorsChanged) && !toggledDoorsCountRangeToToggleMe.Contains(doorsChanged - doorsChangedDelta):
		SetOpen (!open)

func OnSetOpen ():
	pass
