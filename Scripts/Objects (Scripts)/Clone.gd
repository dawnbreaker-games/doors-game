extends Player
class_name Clone

var spawnedBy : CloneDoor
var nextMoveIndex = 0
var nextMove : Vector2i
static var instances : Array[Clone]

func _ready ():
	instances.append(self)
	set_process_input(false)

func NextMove ():
	var moved = nextMoveIndex > 0
	if moved:
		moved = Move(nextMove)
	elif !Player.IsAPlayerAtSpot(nextMove):
		moved = true
	if moved:
		if get_tree() == null:
			return
		nextMoveIndex += 1
		if nextMoveIndex >= pathMovesDict[spawnedBy].size():
			instances.erase(self)
			get_tree().current_scene.remove_child(self)
			queue_free()
			return
		nextMove = pathMovesDict[spawnedBy][nextMoveIndex]
		visible = true

func _exit_tree ():
	instances.erase(self)
