extends Node2D
class_name Weightpad

@export var toggleOnExit : bool
@export var flipDoors : bool
@export var toggleDoors : Array[Door]
static var instances : Array[Weightpad]
