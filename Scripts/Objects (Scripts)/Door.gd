extends Node2D
class_name Door

@export var initOpen : bool
@export var open : bool
@export var affectedByTimeDoors : bool
@export var inspectable : Inspectable
@export var line : Line2D
static var instances : Array[Door]
var startPosition : Vector2
var endPosition : Vector2
var initInspectableText : String
const REPLACE_WITH_OPEN = "_"

func _ready ():
	initInspectableText = inspectable.text
	startPosition = to_global(line.points[0])
	endPosition = to_global(line.points[1])
	SetOpen (open)
	line.use_parent_material = !affectedByTimeDoors

func SetOpen (open : bool):
	var previousOpen = open
	self.open = open
	var openText : String
	if open:
		line.default_color.a = .3
		openText = "open"
	else:
		line.default_color.a = 1 
		openText = "closed"
	inspectable.text = initInspectableText.replace(REPLACE_WITH_OPEN, openText)
	if open != previousOpen:
		OnSetOpen ()

func OnSetOpen ():
	var doorsChangedDelta
	if open != initOpen:
		doorsChangedDelta = 1
	else:
		doorsChangedDelta = -1
	for door in Door.instances:
		var metaDoor = door as MetaDoor
		if metaDoor != null:
			metaDoor.Update (doorsChangedDelta)
